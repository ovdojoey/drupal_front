var DRUPAL = DRUPAL || (function () {

  var container = document.getElementById('step-container'),
    stepTo = Array.prototype.slice.call(document.getElementsByClassName('step__count'));


  function changeStep() {
    container.className = 'step-container activate activate-' + this.dataset.activate; 
  }

  function addClick(elem) {
    elem.addEventListener('click', changeStep, false);
  }

  function init() {
    stepTo.forEach(addClick);
  }

  window.onload = function () {
    setTimeout(function () { container.className = 'step-container activate'; }, 50);
    init();
  };

}());